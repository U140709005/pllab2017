use strict;
use warnings;
my $file = $ARGV[0];
open FILE, '<', $file ;
my @lines = <FILE>;
close FILE;
my %word_freg = ();
foreach my $r(@lines){
	chomp $r;
	foreach my $word(split " ", $r){
		if(!exists $word_freg{$word}) {$word_freg{$word} = 1; }
		else { $word_freg{$word}++; }
		#print "$word\n"	
	}
}

foreach my $key(keys %word_freg){
	print "$key $word_freg{$key}\n";
}